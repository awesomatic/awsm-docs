<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       jordiradstake.nl
 * @since      0.1
 *
 * @package    Awsm_Docs
 * @subpackage Awsm_Docs/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Awsm_Docs
 * @subpackage Awsm_Docs/admin
 * @author     Jordi Radstake <jordi@awesomatic.nl>
 */
class Awsm_Docs_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.1
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.1
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.1
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    0.1
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Awsm_Docs_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Awsm_Docs_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/awsm-docs-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    0.1
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Awsm_Docs_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Awsm_Docs_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/awsm-docs-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Register the post type for the admin area.
	 *
	 * @since    0.2
	 */
	public function register_custom_post_types() {

		/**
		 * This function let you create and edit new pop-ups in the admin area.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Awsm_Popups_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Awsm_Popups_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

			// Set the labels, this variable is used in the $args array
			$labels = array(
				'name'               => __( 'Docs' ),
				'singular_name'      => __( 'Doc' ),
				'add_new'            => __( 'Add New Doc' ),
				'add_new_item'       => __( 'Add New Doc' ),
				'edit_item'          => __( 'Edit Doc' ),
				'new_item'           => __( 'New Doc', 'awesomatic-support' ),
				'all_items'          => __( 'All Docs', 'awesomatic-support' ),
				'view_item'          => __( 'View Doc' ),
				'search_items'       => __( 'Search Docs' ),
				'featured_image'     => 'Img',
				'set_featured_image' => 'Add Img'
			);

			// The arguments for our post type, to be entered as parameter 2 of register_post_type()
			$args = array(
				'labels'            => $labels,

				// The only way to read that field is using this code: $obj = get_post_type_object( 'your_post_type_name' ); echo esc_html( $obj->description );
				//'description'       => 'Type some description ..',

				// Whether to exclude posts with this post type from front end search results
				'public'            => true,

				// Check out all available icons: https://developer.wordpress.org/resource/dashicons/#layout
				'menu_icon'         => 'dashicons-text-page',

				// 26 Comments 2 - Dashboard 4 - Seperator 5 - Posts 10 - Media 15 - Links 20 - Pages - 25 Comments - 59 Seperator - 60 Appaerence - 65 Plugins - 70 Users - 75 Tools - 80 Settings - 99 - Seperator
				'menu_position'     => 208,

				// 'comments' editor has to be true to make us of the Gutenberg editor
				'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields' ),

				// Will be used as slug on the front
				'rewrite'           => array( 'slug' => 'docs', 'with_front' => false ),

				//
				'taxonomies' => array('team-category'),

				//
				'has_archive'       => true,

				// Has to be true to make us of the the Gutenberg editor
				'show_in_rest'      => true,

				//
				'show_in_admin_bar' => true,

				//
				'show_in_nav_menus' => true,

				//
				'has_archive'       => true,

				//
				'query_var'         => 'film'
			);

		// Call the actual WordPress function
		register_post_type( 'awsm-doc', $args);

	}

	/**
	 * Register the taxonomies for the admin area.
	 *
	 * @since    0.2
	 */
	public function register_custom_taxonomies() {

		/**
		 * This function let you create and edit new pop-ups in the admin area.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Awsm_Popups_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Awsm_Popups_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		register_taxonomy(

			// Give it a name, it will be used as slug as well
			'awsm-doc-category',
			
			// Connect it to the custom 'doc' type
			'awsm-doc',
			
			//
			array(
				'label' => __( 'Category' ),
				'rewrite' => array( 'slug' => 'doc-category' ),
				'hierarchical' => true,
			)
			
		);

	}

}
