<?php

/**
 * Fired during plugin deactivation
 *
 * @link       jordiradstake.nl
 * @since      0.1
 *
 * @package    Awsm_Docs
 * @subpackage Awsm_Docs/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      0.1
 * @package    Awsm_Docs
 * @subpackage Awsm_Docs/includes
 * @author     Jordi Radstake <jordi@awesomatic.nl>
 */
class Awsm_Docs_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.1
	 */
	public static function deactivate() {

	}

}
