<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       jordiradstake.nl
 * @since      0.1
 *
 * @package    Awsm_Docs
 * @subpackage Awsm_Docs/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      0.1
 * @package    Awsm_Docs
 * @subpackage Awsm_Docs/includes
 * @author     Jordi Radstake <jordi@awesomatic.nl>
 */
class Awsm_Docs_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    0.1
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'awsm-docs',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
